
CREATE TABLE IF NOT EXISTS "User" (
	"Id"	INTEGER,
	"Name"	INTEGER,
	PRIMARY KEY("Id" AUTOINCREMENT)
);

CREATE TABLE IF NOT EXISTS "Ride" (
	"Id"	INTEGER,
	"Name"	TEXT,
	"Date"	TEXT,
	PRIMARY KEY("Id" AUTOINCREMENT)
);

INSERT INTO "Ride" ("Id", "Name", "Date") VALUES
	(1, 'Melbourne Cup', '2021-11-10'),
	(2, 'World Cup', '2021-11-05');

INSERT INTO "User" ("Id", "Name") VALUES
	(1, "Maxim"),
	(2, "Dmitriy");

CREATE TABLE IF NOT EXISTS "Bet" (
    "BetId"	INTEGER,
	"UserId"	INTEGER,
	"RideId"	INTEGER,
	"Qty"	INTEGER,
	"Condition"	TEXT,
	PRIMARY KEY("BetId" AUTOINCREMENT),
	FOREIGN KEY("RideId") REFERENCES "Ride"("Id"),
	FOREIGN KEY("UserId") REFERENCES "User"("Id")
);

