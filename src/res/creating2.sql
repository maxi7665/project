-- --------------------------------------------------------
-- Хост:                         C:\Users\user\Documents\SQLiteDatabaseBrowserPortable\Rides.db
-- Версия сервера:               3.34.0
-- Операционная система:         
-- HeidiSQL Версия:              11.3.0.6369
-- --------------------------------------------------------



-- Дамп структуры базы данных Rides
-- CREATE DATABASE IF NOT EXISTS "Rides";
--;


-- Дамп структуры для таблица Rides.User
CREATE TABLE IF NOT EXISTS "User" (
	"Id"	INTEGER,
	"Name"	INTEGER,
	PRIMARY KEY("Id" AUTOINCREMENT)
);


-- Дамп структуры для таблица Rides.Ride
CREATE TABLE IF NOT EXISTS "Ride" (
	"Id"	INTEGER,
	"Name"	TEXT,
	"Date"	TEXT,
	PRIMARY KEY("Id" AUTOINCREMENT)
);


-- Дамп структуры для таблица Rides.Bet
CREATE TABLE IF NOT EXISTS "Bet" (
	"UserId"	INTEGER,
	"RideId"	INTEGER,
	"Qty"	INTEGER,
	"Condition"	TEXT,
	FOREIGN KEY("UserId") REFERENCES "User"("Id"),
	FOREIGN KEY("RideId") REFERENCES "Ride"("Id"),
	PRIMARY KEY("UserId","RideId")
);

-- Дамп данных таблицы Rides.Bet: -1 rows
/*!40000 ALTER TABLE "Bet" DISABLE KEYS */;
/*!40000 ALTER TABLE "Bet" ENABLE KEYS */;



-- Дамп данных таблицы Rides.Ride: -1 rows
/*!40000 ALTER TABLE "Ride" DISABLE KEYS */;
INSERT IGNORE INTO "Ride" ("Id", "Name", "Date") VALUES
	(1, 'Melbourne Cup', '2021-11-10'),
	(2, 'World Cup', '2021-11-05');
/*!40000 ALTER TABLE "Ride" ENABLE KEYS */;


-- Дамп данных таблицы Rides.User: -1 rows
/*!40000 ALTER TABLE "User" DISABLE KEYS */;
INSERT IGNORE INTO "User" ("Id", "Name") VALUES
	(1, Maxim),
	(2, Dmitry);
/*!40000 ALTER TABLE "User" ENABLE KEYS */;

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
