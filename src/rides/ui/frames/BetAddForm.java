package rides.ui.frames;

import rides.Utils;
import rides.dao.JpaBetDao;
import rides.domain.Bet;

import javax.swing.*;

public class BetAddForm extends JFrame {
    private JTextField textField1;
    private JPanel panel1;
    private JButton addBetButton;
    private JTextArea textArea1;

    private final int rideId;

    MainFrame caller;

    public BetAddForm(MainFrame main, int rideId) {
        this.caller = main;
        this.rideId = rideId;

        this.setSize(400, 200);
        this.setContentPane(panel1);

        this.setTitle("Соревнование: " + Utils.curRideId + "; Пользователь: " + Utils.curUserId);

        this.setVisible(true);

        this.addBetButton.addActionListener(e -> {
            int qty = 0;
            try {
                qty = Integer.parseInt(textField1.getText());
            } catch (NumberFormatException exception) {
                JOptionPane.showMessageDialog(this, "Вы ввели не число!");
            }

            String condition = textArea1.getText();

            if (qty == 0 || condition.length() == 0) {
                JOptionPane.showMessageDialog(null, "Проверьте ввод!");
            }

            JpaBetDao jbd = new JpaBetDao();

            Bet bet = new Bet();

            bet.setUserId(Utils.curUserId);
            bet.setRideId(this.rideId);
            bet.setCondition(condition);
            bet.setQty(qty);
            jbd.save(bet);

            caller.refresh();

            Utils.curRideId = null;

            this.dispose();
        });

        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

}
