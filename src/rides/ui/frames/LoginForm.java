package rides.ui.frames;

import rides.Utils;
import rides.tableModels.UsersGridModel;

import javax.swing.*;

public class LoginForm extends JFrame {
    private JTable table1;
    private JPanel panel1;
    private JButton loginButton;

    UsersGridModel model = new UsersGridModel();


    public LoginForm() {
        this.setSize(400, 300);
        this.setContentPane(panel1);

        table1.setModel(model);

        this.setVisible(true);

        this.setTitle("Вход");

        loginButton.addActionListener(e -> {

            if (table1.getSelectedRow() != -1) {
                try {
                    Utils.curUserId = (Integer) model.getValueAt(table1.getSelectedRow(), 0);
                } catch (ClassCastException ex) {
                    JOptionPane.showMessageDialog(this, "Ошибка: " + ex.getMessage());
                }

                new MainFrame();

                this.setVisible(false);
            } else {
                JOptionPane.showMessageDialog(this, "Надо выбрать пользователя");
            }
        });

        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        new LoginForm();
    }


}
