package rides.ui.frames;

import rides.Utils;
import rides.domain.Bet;
import rides.tableModels.BetsGridModel;
import rides.tableModels.RidesGridModel;

import javax.swing.*;

public class MainFrame extends JFrame {

    RidesGridModel model;
    BetsGridModel betsModel;
    private JPanel mainPanel;
    private JTable ridesTable;
    private JButton addBetButton;
    private JTabbedPane tabbedPane1;
    private JButton showBets;
    private JTable betsTable;
    private JButton detailsButton;

    public MainFrame() {
        this.setSize(800, 600);
        this.setContentPane(mainPanel);

        // creating models for tabs
        model = new RidesGridModel();
        betsModel = new BetsGridModel();

        ridesTable.setModel(model);
        betsTable.setModel(betsModel);

        this.setTitle("Главная");

        addBetButton.addActionListener(e -> {
            if (ridesTable.getSelectedRow() == -1) {
                JOptionPane.showMessageDialog(this, "Выберите соревнование");
                return;
            }
            try {
                Utils.curRideId = (Integer) model.getValueAt(ridesTable.getSelectedRow(), 0);
            } catch (ClassCastException ex) {
                JOptionPane.showMessageDialog(this, "Ошибка: " + ex.getMessage());
                ex.printStackTrace();
            }

            new BetAddForm(this, Utils.curRideId);
        });

        tabbedPane1.addChangeListener(e -> {
            int pane = tabbedPane1.getSelectedIndex();

            System.out.println(pane);

            if (pane == 1) {
                betsModel.read();
                betsModel.fireTableDataChanged();
                betsTable.setModel(betsModel);

                // notify frame for show new items
                this.addNotify();
            } else if (pane == 0) {
                Utils.curRideId = null;
            }
        });

        showBets.addActionListener(e -> {
            if (ridesTable.getSelectedRow() == -1) {
                JOptionPane.showMessageDialog(this, "Выберите соревнование!");
                return;
            }

            Utils.curRideId = (Integer) ridesTable.getValueAt(ridesTable.getSelectedRow(), 0);
            tabbedPane1.setSelectedIndex(1);
        });

        detailsButton.addActionListener(e -> {
            int selected = betsTable.getSelectedRow();

            if (selected == -1) {
                JOptionPane.showMessageDialog(this, "Выберите ставку!");
            }

            Bet bet = betsModel.getBetAt(selected);

            JOptionPane.showMessageDialog(this, String.format(
                    "Ставка: Пользователь %d\nСоревнование %d\nСумма %d\n Описание %s",
                    bet.getUserId(),
                    bet.getRideId(),
                    bet.getQty(),
                    bet.getCondition()));
        });

        this.setVisible(true);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        new MainFrame();
    }

    public void refresh() {
        model.fireTableDataChanged();
    }
}
