package rides.dao;

import rides.db.DBManager;
import rides.domain.User;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class JpaUserDao implements Dao<User> {

    Connection connection;

    public JpaUserDao() {
        this.connection = DBManager.getConnection();
    }

    public static void main(String[] args) {

    }

    public Optional<User> get(int... id) {
        try {
            Statement stat = connection.createStatement();

            ResultSet res = stat.executeQuery(String.format("select * from user where id = %d limit 1", id[0]));

            User user = new User();
            user.setName(res.getString("Name"));
            user.setId(res.getInt("Id"));

            return Optional.of(user);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }


    public List<User> getAll() {
        try {
            Statement stat = connection.createStatement();

            ResultSet res = stat.executeQuery("select * from user");

            List<User> users = new ArrayList<>();

            while (res.next()) {
                User user = new User();
                user.setName(res.getString("Name"));
                user.setId(res.getInt("Id"));

                users.add(user);
            }

            return users;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void save(User user) {
        try {
            Statement statement = connection.createStatement();

            statement.executeUpdate(String.format("insert into user values(%d,'%s')", user.getId(), user.getName()));

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void update(User user, String[] params) {
        try {
            Statement statement = connection.createStatement();

            statement.executeUpdate(String.format("update user set Name = '%s', where id = %d", user.getName(), user.getId()));

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void delete(User user) {
        try {
            Statement statement = connection.createStatement();

            statement.executeUpdate(String.format("delete user where id = %d", user.getId()));

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
