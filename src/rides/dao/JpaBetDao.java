package rides.dao;

import rides.db.DBManager;
import rides.domain.Bet;

import javax.swing.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class JpaBetDao implements Dao<Bet> {

    Connection connection;

    public JpaBetDao() {
        this.connection = DBManager.getConnection();
    }

    public static void main(String[] args) {

    }

    public Optional<Bet> get(int... ids) {
        try {
            Statement stat = connection.createStatement();

            ResultSet res = stat.executeQuery(String.format("select * from bet where betId = %d limit 1", ids[0]));

            Bet bet = new Bet();
            bet.setRideId(res.getInt("RideId"));
            bet.setRideId(res.getInt("UserId"));
            bet.setQty(res.getInt("Qty"));
            bet.setCondition(res.getString("Condition"));

            return Optional.of(bet);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }


    public List<Bet> getAll() {
        try {
            Statement stat = connection.createStatement();

            ResultSet res = stat.executeQuery("select * from bet");

            List<Bet> bets = new ArrayList<>();

            while (res.next()) {
                Bet bet = new Bet();
                bet.setRideId(res.getInt("RideId"));
                bet.setUserId(res.getInt("UserId"));
                bet.setQty(res.getInt("Qty"));
                bet.setCondition(res.getString("Condition"));

                bets.add(bet);
            }

            return bets;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Bet> getByRideId(int rideId) {
        try {
            Statement stat = connection.createStatement();

            ResultSet res = stat.executeQuery(String.format("select * from bet where rideId = '%d'", rideId));

            List<Bet> bets = new ArrayList<>();

            while (res.next()) {
                Bet bet = new Bet();
                bet.setRideId(res.getInt("RideId"));
                bet.setUserId(res.getInt("UserId"));
                bet.setQty(res.getInt("Qty"));
                bet.setCondition(res.getString("Condition"));

                bets.add(bet);
            }

            return bets;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void save(Bet bet) {
        try {
            Statement statement = connection.createStatement();

            statement.executeUpdate(String.format(
                    "insert into bet (UserId, RideId, Qty, Condition) values('%d', '%d', '%d','%s')",
                    bet.getUserId(),
                    bet.getRideId(),
                    bet.getQty(),
                    bet.getCondition()));

            statement.close();

            connection.commit();

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка!", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }

    public void update(Bet bet, String[] params) {
        try {
            Statement statement = connection.createStatement();

            statement.executeUpdate(String.format(
                    "update  bet set Qty = '%d', Condition = '%s', userId = '%d', rideId = '%d' where betId = '%d'",
                    bet.getQty(),
                    bet.getCondition(),
                    bet.getUserId(),
                    bet.getRideId(),
                    bet.getBetId()));

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void delete(Bet bet) {
        try {
            Statement statement = connection.createStatement();

            statement.executeUpdate(String.format("delete bet where betId = %d", bet.getBetId()));

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
