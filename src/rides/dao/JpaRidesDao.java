package rides.dao;

import rides.db.DBManager;
import rides.domain.Ride;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class JpaRidesDao implements Dao<Ride> {

    Connection connection;

    public JpaRidesDao() {
        this.connection = DBManager.getConnection();
    }

    public static void main(String[] args) {

    }

    @Override
    public Optional<Ride> get(int... id) {
        try {
            Statement stat = connection.createStatement();

            ResultSet res = stat.executeQuery(String.format("select * from ride where id = %d limit 1", id[0]));

            Ride ride = new Ride();
            ride.setDate(res.getString("Date"));
            ride.setName(res.getString("Name"));
            ride.setId(res.getInt("Id"));

            return Optional.of(ride);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public List<Ride> getAll() {
        try {
            Statement stat = connection.createStatement();

            ResultSet res = stat.executeQuery("select * from ride");

            List<Ride> rides = new ArrayList<>();

            while (res.next()) {
                Ride ride = new Ride();
                ride.setDate(res.getString("Date"));
                ride.setName(res.getString("Name"));
                ride.setId(res.getInt("Id"));

                rides.add(ride);
            }

            return rides;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void save(Ride ride) {
        try {
            Statement statement = connection.createStatement();

            statement.executeUpdate(String.format("insert into ride values(%d,'%s','%s')", ride.getId(), ride.getName(), ride.getDate()));

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Ride ride, String[] params) {
        try {
            Statement statement = connection.createStatement();

            statement.executeUpdate(String.format("update ride set Name = '%s', Date = '%s', where id = %d", ride.getName(), ride.getDate(), ride.getId()));

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Ride ride) {
        try {
            Statement statement = connection.createStatement();

            statement.executeUpdate(String.format("delete ride where id = %d", ride.getId()));

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
