package rides.dao;

import rides.db.DBManager;

import javax.swing.*;
import java.util.List;
import java.util.Optional;

public interface Dao<T> {

    Optional<T> get(int... ids);

    List<T> getAll();

    void save(T t);

    void update(T t, String[] params);

    void delete(T t);

    default void close() {
        DBManager.close();
    }

    default void showError(Throwable thr) {
        JOptionPane.showMessageDialog(null, thr.getMessage());
    }
}