package rides.tableModels;

import rides.Utils;
import rides.dao.JpaBetDao;
import rides.domain.Bet;

import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import java.util.List;

public class BetsGridModel extends AbstractTableModel {

    List<Bet> list;

    JpaBetDao betsDao;


    public BetsGridModel() {
        this.betsDao = new JpaBetDao();

        this.read();
    }

    public void read() {
        if (Utils.curRideId == null) {
            list = betsDao.getAll();
        } else {
            list = betsDao.getByRideId(Utils.curRideId);
        }
    }

    public void dispose() {
        betsDao.close();
    }

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return switch (columnIndex) {
            case 0 -> "Пользователь";
            case 1 -> "Соревнование";
            case 2 -> "Сумма";
            case 3 -> "Условие";
            default -> "";
        };
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return switch (columnIndex) {
            case 0 -> list.get(rowIndex).getUserId();
            case 1 -> list.get(rowIndex).getRideId();
            case 2 -> list.get(rowIndex).getQty();
            case 3 -> list.get(rowIndex).getCondition();
            default -> "";
        };
    }

    public Bet getBetAt(int rowIndex) {
        if (rowIndex < list.size()) {
            return list.get(rowIndex);
        }

        return null;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

    }

    @Override
    public void addTableModelListener(TableModelListener l) {

    }

    @Override
    public void removeTableModelListener(TableModelListener l) {


    }
}
