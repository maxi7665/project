package rides.tableModels;

import rides.dao.JpaRidesDao;
import rides.domain.Ride;

import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import java.util.List;

public class RidesGridModel extends AbstractTableModel {

    List<Ride> list;

    JpaRidesDao ridesDao;

    public RidesGridModel() {
        this.ridesDao = new JpaRidesDao();

        list = ridesDao.getAll();
    }

    public void dispose() {
        ridesDao.close();
    }

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return switch (columnIndex) {
            case 0 -> "ИД";
            case 1 -> "Имя";
            case 2 -> "Дата";
            default -> "";
        };
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return switch (columnIndex) {
            case 0 -> list.get(rowIndex).getId();
            case 1 -> list.get(rowIndex).getName();
            case 2 -> list.get(rowIndex).getDate();
            default -> "";
        };
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

    }

    @Override
    public void addTableModelListener(TableModelListener l) {

    }

    @Override
    public void removeTableModelListener(TableModelListener l) {


    }

    @Override
    public void fireTableDataChanged() {
        super.fireTableDataChanged();
    }
}
