package rides.db;

import org.apache.ibatis.jdbc.ScriptRunner;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBManager {

    static Connection connection;


    public static Connection getConnection() {

        if (connection != null) {
            return connection;
        }
        try {
            Class.forName("org.sqlite.JDBC");

            connection = DriverManager.getConnection("jdbc:sqlite:src/res/rides.db");

            BufferedReader reader = new BufferedReader(new FileReader("src/res/creating.sql"));

            ScriptRunner runner = new ScriptRunner(connection);
            runner.setEscapeProcessing(false);
            runner.runScript(reader);

            return connection;

        } catch (SQLException | ClassNotFoundException | FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void close() {
        try {
            connection.close();
            connection = null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
